# Imports
import requests, json

# Functions
async def fetchAPI(apiURL):
	output = requests.get(apiURL).json()

	return output