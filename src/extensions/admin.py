import discord
from discord.ext import commands
import datetime as dt
from datetime import datetime

class AdminCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # Server Prefix Commands
    @commands.command(aliases=[sp])
    async def serverprefix(bot, ctx, prefix = None):
        if prefix == None: # Shows the prefix in an embed message
            dbprefix = db.getprefix(ctx.guild.id)
            prefixhistory = db.prefixhistory(ctx.guild.id)
            embed = discord.Embed(title=f"Server prefix for {server.name}", description=None, colour=0x98FB98)
            embed.add_field(name="Current Prefix", value=f"{dbprefix}" or ">>> (*default prefix* )")
            embed.add_field(name="Prefix History", value=f"{prefixhistory}" or "There is no prefix history for this server.")
            embed.set_footer(text=f'Info Called by {ctx.author.name}#{ctx.author.discriminator} at {dt.datetime.now().date()}', icon_url=ctx.author.avatar_url_as(size=256))

            await ctx.send(embed)
        else: # Sets or updates the prefix
            db.setprefix(ctx.guild.id, prefix)

            embed = discord.Embed(title="\uFEFF", description=None, colour=0x98FB98)
            embed.add_field(name=f"Your new server prefix is ``{prefix}``")
            embed.set_footer(text=f'Info Called by {ctx.author.name}#{ctx.author.discriminator} at {dt.datetime.now().date()}', icon_url=ctx.author.avatar_url_as(size=256))

            await ctx.send(embed)

    # Modules
    @commands.group(name="module", pass_context=True, invoke_without_command=True)
    async def _module(bot, ctx): # returns info about each module
        pass

    @_module.command(name="status") # lists all enables and disabled modules via an embed
    async def m_status(bot, ctx):
        nothing = None
        enabled, disabled = db.checkmodules(ctx.guild.id)

    @_module.command(name="toggle")
    async def m_toggle(bot, ctx, cogname, order):
        if order == 'on':
            pass
        elif order == 'off':
            pass
        else:
            pass

    # Bot Roles
    @commands.command(name="addadmin", aliases=[aa])
    async def _addadmin(bot, ctx, user: discord.Member):
        pass

    @commands.command(name="removeadmin", aliases=[ra])
    async def _removeadmin(bot, ctx, user: discord.Member):
        pass

    @commands.command(name="addofficer", aliases=[ao])
    async def _addofficer(bot, ctx, user: discord.Member):
        pass

    @commands.command(name="removeofficer", aliases=[ro])
    async def _removeofficer(bot, ctx, user: discord.Member):
        pass

    @commands.command(name="addmod", aliases=[am])
    async def _addmod(bot, ctx, user: discord.Member):
        pass

    @commands.command(name="removemod", aliases=[rm])
    async def _removemod(bot, ctx, user: discord.Member):
        pass

    @commands.command(name="addauthuser", aliases=[aau])
    async def _addauthuser(bot, ctx, user: discord.Member):
        pass

    @commands.command(name="removeauthuser", aliases=[rau])
    async def _removeauthuser(bot, ctx, user: discord.Member):
        pass
        
    @commands.command()
    async def createrole(self, ctx, rolename):
    	server = ctx.guild
    	try:
    		role = await server.create_role(name=rolename)
    	except Exception as e:
    		await ctx.send("I don't have the perms to make roles!<:cry:441942123047288832>")
    	else:
    		embed = discord.Embed(title=f"Role Successfully Created!", description=None, colour=0x98FB98)
    		embed.add_field(name="Role Name", value=f"{role}")
    		embed.add_field(name="Role ID", value=f"{role.id}")
    		embed.set_footer(text=f'Command called by {ctx.author.name}#{ctx.author.discriminator} at {dt.datetime.now().date()}', icon_url=ctx.author.avatar_url_as(size=256))
    		
            await ctx.send(embed=embed)

    @commands.command()
    async def deleterole(self, ctx, rolename):
    	for role in ctx.guild.roles:
    		if role.name == rolename:
    			therole = role
    	rolename = therole.name
    	roleid = therole.id
    	try:
    		await therole.delete()
    	except Exception as e:
    		await ctx.send("I don't have the perms to delete roles!<:cry:441942123047288832>")
    	else:
    		embed = discord.Embed(title=f"Role Successfully Deleted!", description=None, colour=0x98FB98)
    		embed.add_field(name="Role Name", value=f"{rolename}")
    		embed.add_field(name="Role ID", value=f"{roleid}")
    		embed.set_footer(text=f'Command called by {ctx.author.name}#{ctx.author.discriminator} at {dt.datetime.now().date()}', icon_url=ctx.author.avatar_url_as(size=256))
    		await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(AdminCog(bot))