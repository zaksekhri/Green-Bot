# Imports
import discord
from discord.ext import commands
from ..functions.embeds import embedC
from ..functions.general import fetchAPI
from ..functions.date import Date

# CLass
class WarframeCog(commands.Cog):
	"""docstring for WarframeCog"""
	def __init__(self, bot):
		super(WarframeCog, self).__init__()
		self.bot = bot

	base_api = "https://api.warframestat.us/pc"
	api_extentions = {
		"cetus" : "/cetusCycle",
		"vallis" : "/vallisCycle",
		"cambion" : "/cambionCycle",
		"arbi" : "/arbitration",
		"kuva" : "/kuva",
		"sortie" : "/sortie",
		"fissures" : "/fissures",
		"invasions" : "/invasions",
		"events" : "/events"
	}

	async def fetchWFAPI(self, extention = None):
		url = self.base_api 
		if extention is not None:
			url += self.api_extentions[extention]

		warframe_api = await fetchAPI(url)

		return warframe_api

	@commands.command()
	async def current_arbi(self, ctx):
		arbi_info = await self.fetchWFAPI("arbi")

		embed = embedC.quick_embed("Current Arbitration", None, 0x98FB98)

		fields = [
			{"name" : "Enemy", "value" : arbi_info["enemy"], "inline" : False},
			{"name" : "Mission", "value" : arbi_info["type"], "inline" : False},
			{"name" : "Node", "value" : arbi_info["node"], "inline" : False},
			{"name" : "Expiry", "value" : arbi_info["expiry"] , "inline" : False},
			{"name" : "Archwing", "value" : arbi_info["archwing"], "inline" : False}
		]

		embedC().builder(embed, ctx.author, fields)

		await ctx.send(content=None,embed=embed)

	@commands.command()
	async def current_sortie(self, ctx):
		sortie_info = await self.fetchWFAPI("sortie")

		embed = embedC.quick_embed("Current Sortie", None, 0x98FB98)

		fields = [
			{"name" : "Enemy", "value" : sortie_info["enemy"], "inline" : False},
			{"name" : "Mission", "value" : sortie_info["type"], "inline" : False},
			{"name" : "Node", "value" : sortie_info["node"], "inline" : False},
			{"name" : "Expiry", "value" : sortie_info["expiry"] , "inline" : False},
			{"name" : "Archwing", "value" : sortie_info["archwing"], "inline" : False}
		]

		embedC().builder(embed, ctx.author, fields)

		await ctx.send(content=none, embed=embed)

	@commands.command(name="server-index", aliases=['server-list'])
	async def server_index(self, ctx):
		embed = embedC.quick_embed(f"Server Index", None, 0x98FB98)

		fields = [
			{"name" : "General Warframe Servers", "value" : "\
			[Official Warframe Server](https://discord.gg/cV6KV3G)\
			\n[Community Warframe Server](https://discord.gg/warframe)\
			\n[Warframe Trading Hub](https://discord.gg/EwD6J37)\
			\n[Warframe Blessings](https://discord.gg/3hHy5ygR4y)\
			\n[Warframe Giveaways](https://discord.com/invite/d8ZYADy)\
			\n[Endgame Community](https://discord.gg/2REYJrK)", "inline": True},
			{"name" : "Topical Warframe Servers", "value" : "\
			[Warframe University](https://discord.gg/ftfPKjP)\
			\n[Warframe Speedruns](https://discord.gg/7wtcKvv)\
			\n[Riven Info and Trading](https://discord.gg/S7aCrWx)\
			\n[Eidolon Zone](https://discord.gg/jDkrGf7)\
			\n[Warframe Arbitrations](https://discord.gg/ENRWGZr)\
			\n[Warframe Railjack](https://discord.gg/JvYVMNa)\
			\n[Warframe Conclave](https://discord.gg/asJsw6Q)\
			\n[Kubrow & Kavat Breeding and Trading](https://discord.gg/abzV2Cb)", "inline": True}
		]

		embedC().builder(embed, ctx.author, fields)

		await ctx.send(embed=embed)

def setup(bot):
	bot.add_cog(WarframeCog(bot))